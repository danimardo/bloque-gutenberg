/**
 * Ejemplo de bloque estático
 *
 * https://bitbucket.org/danimardo/bloque-gutenberg/src/master/bloque.js
 */
( function() {
    var __ = wp.i18n.__; //  __() Para la internacionalización
    var createElement = wp.element.createElement; // wp.element.createElement() sirve para crear elementos
    var registerBlockType = wp.blocks.registerBlockType; // registerBlockType() sirve para registrar bloques

    /**
     * Registrar bloque
     *
     * @param  {string}   name     Nombre del bloque.
     * @param  {Object}   parámetros de configuración del bloque.
     * @return {?WPBlock}          El bloque en si mismo, si se registra correctamente,
     *                             de otra forma "undefined".
     */
    
    registerBlockType(
        'mi-bloque/bloque-estatico', // El nombre del bloque. Es una cadena que contiene un namespace como prefijo. Por ejemplo: mi-bloque/bloque-estatico.
        {
            title: __( 'Bloque estático de ejemplo' ), // Titulo que veremos. la función __() permite la internacionalización.
            icon: 'dashicons-smiley', // icono de Dashicons. https://developer.wordpress.org/resource/dashicons/.
            category: 'common', // Caegoría del bloque. Puede ser: common, formatting, layout widgets, embed.

            // Define el bloque para el editor
            edit: function( props ) {
                return createElement(
                    'p', // Tipo de tag. En este caso un párrafo
                    {
                        className: props.className,  // Nombre de la clase que es generada utilizando el nombre del bloque como frefijo con wp-block-, y en vez de utilizar el / del namespace se utiliza -.
                    },
                    'Wordpress con Gutenberg es maravilloso' // Contenido del bloque
                );
            },

            // Definimos la forma de guardar
            save: function( props ) {
                return createElement(
                    'p', // Tipo de tag. En este caso un párrafo
                    {
                        className: props.className,  // Nombre de la clase que es generada utilizando el nombre del bloque como frefijo con wp-block-, y en vez de utilizar el / del namespace se utiliza -.
                    },
                    'Wordpress con Gutenberg es maravilloso' // Contenido del bloque
                );
            },
        }
    );
})();
