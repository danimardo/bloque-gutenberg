<?php
/**
 * Plugin Name: Bloque Gutenberg: Mi Bloque
 * Plugin URI: https://bitbucket.org/danimardo/bloque-gutenberg
 * Description: Un ejemplo de un bloque muy simple: Un párrado con un texto
 * Author: mardomingo
 * Author URI: https://mardomingo.es
 * Version: 1.0.0
 * License: GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// Salimos si se accede directamente.
if ( ! defined( 'ABSPATH' ) ) {
   exit;
}

/**
  * Encola en el editor los recursos del bloque.
 * wp-blocks: registerBlockType() Es el método para registar bloques.
 * wp-element: wp.element.createElement() es el método para crear elementos.
 * wp-i18n:    __() es un método para la internacionalización.
 *
 */
function mdlr_static_block_example_backend_enqueue() {
   wp_enqueue_script(
      'mdlr-static-block-example-backend-script', // Manejador único
      plugins_url( 'bloque.js', __FILE__ ), // bloque.js: Es aquí donde registramos el bloque
      array( 'wp-blocks', 'wp-i18n', 'wp-element' ), // Dependencias
      filemtime( plugin_dir_path( __FILE__ ) . 'bloque.js' )
       modification time.
   );
}
add_action( 'enqueue_block_editor_assets', 'mdlr_static_block_example_backend_enqueue' );
